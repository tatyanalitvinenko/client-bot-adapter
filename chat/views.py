from apiai import apiai
from django.http import JsonResponse

from helloworld.settings import CLIENT_ACCESS_TOKEN


def about(request):
    message = request.POST.get('message')
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)

    request = ai.text_request()

    request.session_id = "1234567891234567"

    request.query = message

    response = request.getresponse()

    print(response.read())
    return JsonResponse({'response': ''})
